﻿using System;
using System.Windows.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ColorTests
{
    [TestClass]
    public class RGB_HSV_Tests
    {
        [TestMethod]
        public void RGB_HSV_Black()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(0, 0, 0));

            Assert.AreEqual(0.0, hsv.Hue);
            Assert.AreEqual(0.0, hsv.Saturation, 0.005);
            Assert.AreEqual(0.0, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_White()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(255, 255, 255));

            Assert.AreEqual(0.0, hsv.Hue);
            Assert.AreEqual(0.0, hsv.Saturation, 0.005);
            Assert.AreEqual(1.0, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Red()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(255, 0, 0));

            Assert.AreEqual(0.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(1.0, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Lime()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(0, 255, 0));

            Assert.AreEqual(120.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(1.0, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Blue()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(0, 0, 255));

            Assert.AreEqual(240.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(1.0, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Yellow()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(255, 255, 0));

            Assert.AreEqual(60.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(1.0, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Cyan()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(0, 255, 255));

            Assert.AreEqual(180.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(1.0, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Magenta()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(255, 0, 255));

            Assert.AreEqual(300.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(1.0, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Silver()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(192, 192, 192));

            Assert.AreEqual(0.0, hsv.Hue);
            Assert.AreEqual(0.0, hsv.Saturation, 0.005);
            Assert.AreEqual(0.75, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Gray()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(127, 127, 127));

            Assert.AreEqual(0.0, hsv.Hue);
            Assert.AreEqual(0.0, hsv.Saturation, 0.005);
            Assert.AreEqual(0.5, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Maroon()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(127, 0, 0));

            Assert.AreEqual(0.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(0.5, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Olive()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(127, 127, 0));

            Assert.AreEqual(60.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(0.5, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Green()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(0, 127, 0));

            Assert.AreEqual(120.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(0.5, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Purple()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(127, 0, 127));

            Assert.AreEqual(300.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(0.5, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Teal()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(0, 127, 127));

            Assert.AreEqual(180.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(0.5, hsv.Value, 0.005);
        }
        [TestMethod]
        public void RGB_HSV_Navy()
        {
            var hsv = new _50ShadesOfColor.ColorHSV(Color.FromRgb(0, 0, 127));

            Assert.AreEqual(240.0, hsv.Hue);
            Assert.AreEqual(1.0, hsv.Saturation, 0.005);
            Assert.AreEqual(0.5, hsv.Value, 0.005);
        }

    }
}
