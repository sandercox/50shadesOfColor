﻿using System;
using System.Windows.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ColorTests
{
    [TestClass]
    public class HSV_RGB_Tests
    {
        [TestMethod]
        public void HSV_RGB_Black()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(0.0f, 0.0f, 0.0f).Color;

            Assert.AreEqual(0, rgb.R);
            Assert.AreEqual(0, rgb.G);
            Assert.AreEqual(0, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_White()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(0.0f, 0.0f, 1.0f).Color;

            Assert.AreEqual(255, rgb.R);
            Assert.AreEqual(255, rgb.G);
            Assert.AreEqual(255, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Red()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(0.0f, 1.0f, 1.0f).Color;

            Assert.AreEqual(255, rgb.R);
            Assert.AreEqual(0, rgb.G);
            Assert.AreEqual(0, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Lime()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(120.0f, 1.0f, 1.0f).Color;

            Assert.AreEqual(0, rgb.R);
            Assert.AreEqual(255, rgb.G);
            Assert.AreEqual(0, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Blue()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(240.0f, 1.0f, 1.0f).Color;

            Assert.AreEqual(0, rgb.R);
            Assert.AreEqual(0, rgb.G);
            Assert.AreEqual(255, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Yellow()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(60.0f, 1.0f, 1.0f).Color;

            Assert.AreEqual(255, rgb.R);
            Assert.AreEqual(255, rgb.G);
            Assert.AreEqual(0, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Cyan()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(180.0f, 1.0f, 1.0f).Color;

            Assert.AreEqual(0, rgb.R);
            Assert.AreEqual(255, rgb.G);
            Assert.AreEqual(255, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Magenta()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(300.0f, 1.0f, 1.0f).Color;

            Assert.AreEqual(255, rgb.R);
            Assert.AreEqual(0, rgb.G);
            Assert.AreEqual(255, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Silver()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(0.0f, 0.0f, 0.75f).Color;

            Assert.AreEqual(191, rgb.R);
            Assert.AreEqual(191, rgb.G);
            Assert.AreEqual(191, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Gray()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(0.0f, 0.0f, 0.50f).Color;

            Assert.AreEqual(127, rgb.R);
            Assert.AreEqual(127, rgb.G);
            Assert.AreEqual(127, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Maroon()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(0.0f, 1.0f, 0.50f).Color;

            Assert.AreEqual(127, rgb.R);
            Assert.AreEqual(0, rgb.G);
            Assert.AreEqual(0, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Olive()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(60.0f, 1.0f, 0.50f).Color;

            Assert.AreEqual(127, rgb.R);
            Assert.AreEqual(127, rgb.G);
            Assert.AreEqual(0, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Green()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(120.0f, 1.0f, 0.50f).Color;

            Assert.AreEqual(0, rgb.R);
            Assert.AreEqual(127, rgb.G);
            Assert.AreEqual(0, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Purple()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(300.0f, 1.0f, 0.50f).Color;

            Assert.AreEqual(127, rgb.R);
            Assert.AreEqual(0, rgb.G);
            Assert.AreEqual(127, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Teal()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(180.0f, 1.0f, 0.50f).Color;

            Assert.AreEqual(0, rgb.R);
            Assert.AreEqual(127, rgb.G);
            Assert.AreEqual(127, rgb.B);
        }
        [TestMethod]
        public void HSV_RGB_Navy()
        {
            var rgb = new _50ShadesOfColor.ColorHSV(240.0f, 1.0f, 0.5f).Color;
            Assert.AreEqual(0, rgb.R);
            Assert.AreEqual(0, rgb.G);
            Assert.AreEqual(127, rgb.B);
        }
    }
}
