﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _50ShadesOfColor
{
    public class ColorHSV
    {
        public float Hue;
        public float Saturation;
        public float Value;

        public Color Color
        {
            get
            {
                // convert to rgb color
                float r = 0;
                float g = 0;
                float b = 0;

                float c = Value * Saturation;
                float x = c * (1 - (Math.Abs(((Hue / 60) % 2) - 1)));
                float m = Value - c;

                if (Hue < 60)
                {
                    r = c;
                    g = x;
                    b = 0;
                }
                else if(Hue < 120)
                {
                    r = x;
                    g = c;
                    b = 0;
                }
                else if (Hue < 180)
                {
                    r = 0;
                    g = c;
                    b = x;
                }
                else if (Hue < 240)
                {
                    r = 0;
                    g = x;
                    b = c;
                }
                else if (Hue < 300)
                {
                    r = x;
                    g = 0;
                    b = c;
                }
                else if (Hue < 360)
                {
                    r = c;
                    g = 0;
                    b = x;
                }

                return Color.FromRgb((byte)(int)((r + m) * 255), (byte)(int)((g + m) * 255), (byte)(int)((b + m) * 255));
            }
        }

        public ColorHSV(float hue, float saturation, float value)
        {
            Hue = hue;
            Saturation = saturation;
            Value = value;
        }

        public ColorHSV(Color color)
        {
            float r = color.R / 255.0f;
            float g = color.G / 255.0f;
            float b = color.B / 255.0f;
            float cMax = Math.Max(r, Math.Max(g, b));
            float cMin = Math.Min(r, Math.Min(g, b));
            float delta = cMax - cMin;

            if (delta == 0)
                Hue = 0;
            else if (cMax == r)
                Hue = ((g - b) / delta) % 6;
            else if (cMax == g)
                Hue = ((b - r) / delta) + 2;
            else if (cMax == b)
                Hue = ((r - g) / delta) + 4;

            if (Hue < 0)
                Hue += 6;
            Hue = Hue * 60;

            if (cMax == 0)
                Saturation = 0;
            else
                Saturation = delta / cMax;

            Value = cMax;
        }

    }

    public class LabelPropertyClass
    {
        public static readonly DependencyProperty LabelProperty =
          DependencyProperty.RegisterAttached(
            "Label",
            typeof(string),
            typeof(LabelPropertyClass),
            new FrameworkPropertyMetadata(
              string.Empty, FrameworkPropertyMetadataOptions.Inherits));

        public static void SetLabelProperty(UIElement element, string value)
        {
            element.SetValue(LabelProperty, value);
        }

        public static string GetLabelProperty(UIElement element)
        {
            return (string)element.GetValue(LabelProperty);
        }
    }

    public class Shade : System.ComponentModel.INotifyPropertyChanged
    {
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string property = null)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(property));
            }
        }

        private Color _Color;
        private ColorHSV _ColorHSV;
        public Color Color
        {
            get { return _Color; }
            set
            {
                _Color = value;
                _ColorHSV = new ColorHSV(_Color);
                NotifyPropertyChanged();
                NotifyPropertyChanged("Brush");
                NotifyPropertyChanged("RGBText");
                NotifyPropertyChanged("HSVText");
            }
        }

        public Brush Brush
        {
            get
            {
                return new SolidColorBrush(Color);
            }
        }

        public string RGBText
        {
            get
            {
                return $"R={Color.R:#,0}, G={Color.G:#,0}, B={Color.B:#,0}";
            }
        }

        public string HSVText
        {
            get
            {
                return $"H={_ColorHSV.Hue:0.0}, S={_ColorHSV.Saturation:0.0}, V={_ColorHSV.Value:0.0}";
            }
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, System.ComponentModel.INotifyPropertyChanged
    {
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string property = null)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(property));
            }
        }

        private System.Collections.ObjectModel.ObservableCollection<Shade> _Shades = new System.Collections.ObjectModel.ObservableCollection<Shade>();
        public System.Collections.ObjectModel.ObservableCollection<Shade> Shades
        {
            get { return _Shades; }
            set
            {
                _Shades = value;
                NotifyPropertyChanged();
            }
        }

        private Double _Red = 50;
        public Double Red
        {
            get { return _Red; }
            set
            {
                _Red = value;
                UpdateShades();
                NotifyPropertyChanged();
                NotifyPropertyChanged("Hue");
                NotifyPropertyChanged("Saturation");
                NotifyPropertyChanged("Value");
                NotifyPropertyChanged("ColorBrush");
            }
        }

        private Double _Green;
        public Double Green
        {
            get { return _Green; }
            set
            {
                _Green = value;
                UpdateShades();
                NotifyPropertyChanged();
                NotifyPropertyChanged("Hue");
                NotifyPropertyChanged("Saturation");
                NotifyPropertyChanged("Value");
                NotifyPropertyChanged("ColorBrush");
            }
        }

        private Double _Blue;
        public Double Blue
        {
            get { return _Blue; }
            set
            {
                _Blue = value;
                UpdateShades();
                NotifyPropertyChanged();
                NotifyPropertyChanged("Hue");
                NotifyPropertyChanged("Saturation");
                NotifyPropertyChanged("Value");
                NotifyPropertyChanged("ColorBrush");
            }
        }

        public Brush ColorBrush
        {
            get { return new SolidColorBrush(Color.FromRgb((byte)Red,(byte)Green,(byte)Blue)) ; }
        }

        public Brush HSVBrush
        {
            get { return new SolidColorBrush(_hsv.Color); }
        }

        private ColorHSV _hsv = new ColorHSV(0,0,0);
        public Double Hue { get => _hsv.Hue; }
        public Double Saturation { get => _hsv.Saturation; }
        public Double Value { get => _hsv.Value; }

        private int _ColorCount = 10;
        public int ColorCount
        {
            get { return _ColorCount; }
            set
            {
                _ColorCount = value;
                UpdateShades();
                NotifyPropertyChanged();
            }
        }

        public MainWindow()
        {
            this.DataContext = this;
            InitializeComponent();

            for(int i = 0; i < 15; ++i)
            {
                Shades.Add(new Shade() { Color = Color.FromRgb(123, (byte)(255/15.0 * i), 123) });
            }
        }

        protected void UpdateShades()
        {
            var hsv = new ColorHSV(Color.FromRgb((byte)Red, (byte)Green, (byte)Blue));
            _hsv = hsv;
            NotifyPropertyChanged("HSVBrush");

            Shades.Clear();
            float factor = (1 - hsv.Value) / hsv.Saturation;


            for(int i =0; i < ColorCount; ++i)
            {
                float step = 1.0f / ColorCount * i;
                float h = hsv.Hue;
                float s = step;
                float v = 1 - (step * factor);
                if (v < 0) v = 0;
                if (v > 1) v = 1;
                var newHsv = new ColorHSV(h, s, v);
                Shades.Add(new Shade() { Color = newHsv.Color });
            }
        }
    }
}
